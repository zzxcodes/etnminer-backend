package xyz.etnminer.utils;

import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import xyz.etnminer.api.subscribers.SubscriberApi;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MemCache {

    private static final int CACHE_EXPIRY_SECONDS = 2 * 60;
    private static Logger logger = Logger.getLogger(SubscriberApi.class.getSimpleName());
    private static Cache cache;

    static {
        initMemCache();
    }

    private static void initMemCache() {
        try {
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            Map<Object, Object> properties = new HashMap<>();
            properties.put(GCacheFactory.EXPIRATION_DELTA, TimeUnit.SECONDS.toSeconds(CACHE_EXPIRY_SECONDS));
            cache = cacheFactory.createCache(properties);
        } catch (CacheException e) {
            logger.log(Level.SEVERE, "Initializing mem cache failed", e);
        }
    }

    public static Cache cache() {
        return cache;
    }
}
