package xyz.etnminer.common;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import xyz.etnminer.entities.subscribers.Subscriber;

/**
 * Created by adi on 3/5/17.
 */
public class OfyService {
    static {
        ObjectifyService.register(Subscriber.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}
