package xyz.etnminer.entities.subscribers;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Subscriber {
    @Id
    public String email_address;
}
