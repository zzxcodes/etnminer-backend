package xyz.etnminer.api.subscribers;

import com.google.api.server.spi.config.ApiMethod;
import endpoints.repackaged.org.jose4j.base64url.Base64;
import xyz.etnminer.api.Base;
import xyz.etnminer.common.Constants;
import xyz.etnminer.common.OfyService;
import xyz.etnminer.entities.subscribers.Subscriber;
import xyz.etnminer.entities.subscribers.SubscriberCountResult;
import xyz.etnminer.utils.MemCache;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SubscriberApi extends Base {

    private static final String SUBSCRIBER_COUNT = "subscriberCount";
    private static Logger logger = Logger.getLogger(SubscriberApi.class.getSimpleName());

    @ApiMethod(httpMethod = ApiMethod.HttpMethod.POST, path = Constants.SUBSCRIBERS_PATH)
    public SubscriberCountResult createSubscriber(Subscriber subscriber) {
        SubscriberCountResult subscriberCountResult = new SubscriberCountResult();
        try {
            validateInput(subscriber);

            String auth = "Basic " + Base64.encode(new String("gg" + ":" + "ef940258d0138b5d8caf82d21ec5e70b-us16").getBytes());
            String url = "https://us16.api.mailchimp.com/3.0/lists/ce98527099/members/";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", auth);

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            String req = "{ \"email_address\":" + "\"" + subscriber.email_address + "\"" + ", \"status\":\"subscribed\" }";
            wr.writeBytes(req);
            wr.flush();
            wr.close();


            int count = loadSubscriberCountFromCache();
            int responseCode = con.getResponseCode();
            if (responseCode != 200 || responseCode != 201 || responseCode != 204) {
                logger.severe("Writing to mailchimp failed with error code : " + responseCode);
            } else {
                // store in data store
                OfyService.ofy().save().entity(subscriber).now();
                MemCache.cache().put(SUBSCRIBER_COUNT, ++count);
            }
            subscriberCountResult.count = count;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return subscriberCountResult;
    }

    @ApiMethod(httpMethod = ApiMethod.HttpMethod.GET, path = Constants.SUBSCRIBERS_PATH)
    public SubscriberCountResult getSubscriberCount() {
        int count = loadSubscriberCountFromCache();
        SubscriberCountResult subscriberCountResult = new SubscriberCountResult();
        subscriberCountResult.count = count;
        return subscriberCountResult;
    }

    private int loadSubscriberCountFromCache() {
        int count = 0;
        try {
            Object countObj = MemCache.cache().get(SUBSCRIBER_COUNT);
            if (countObj == null) {
                count = OfyService.ofy().load().type(Subscriber.class).keys().list().size();
                MemCache.cache().put(SUBSCRIBER_COUNT, count);
            } else {
                count = (Integer) countObj;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return count;
    }

    private void validateInput(Subscriber subscriber) throws AddressException {
        String email = subscriber.email_address;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException e) {
            throw e;
        }

    }

}
